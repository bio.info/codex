"""
Copyright (c) 2018, Giulio Piemontese, <gpiemont@github.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by Giulio Piemontese.
4. Neither the name of Giulio Piemontese nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

#
# State Machine translation table 
#   Codon => Amminoacid
#   
# Credits to http://rosalind.info 
#   and Open Clip Art
#

sm = {

    'U' : {
	'A' : {
            'UC' : [ 'Y', 'Tyr', 'Tyrosina' ],
            'AG' : [ '*', 'STP', "Stop", ],
        },
	'C' : {
            'UCAG' : [ 'S', 'Ser', 'Serine' ],
        },
	'G' : {
            'UC' : [ 'C', 'Cys', 'Cysteina' ],
            'A'  : [ '*', 'STP', "Stop" ],
            'G'  : [ 'W', 'Trp', 'Tryptophan' ],
        },
	'U' : {
            'UC' : [ 'F', 'Phe', 'Phenylanine' ],
            'AG' : [ 'L', 'Leu', 'Leucine' ],
        },
    },
 
    'C' : { 
        'A' : { 
            'UC' : [ 'H', 'His', 'Histidine' ],
            'AG' : [ 'Q', 'Gln', 'Glutamine' ],
        },
        'C' : { 
            'UCAG' : [ 'P', 'Pro', 'Proline' ],
        },
        'G' : { 
            'UCAG' : [ 'R', 'Arg', 'Arginine' ],
        },
        'U' : { 
            'UCAG' : [ 'L', 'Leu', 'Leucine' ],
        },
    },  
    
    'A' : { 
        'A' : { 
            'UC' : [ 'N', 'Asn', 'Asparagine' ],
            'AG' : [ 'K', 'Lys', 'Lysine' ],
        },
        'C' : { 
            'UCAG' : [ 'T', 'Thr', 'Threonine' ],
        },
        'G' : { 
            'UC'   : [ 'S', 'Ser', 'Serine' ],
            'AG'   : [ 'R', 'Arg', 'Arginine' ],
        },
        'U' : {
            'UCA' : [ 'I', 'Ile', 'Isoleucine' ],
            'G'   : [ 'M', 'Met', 'Methionine' ],
        },
    },

    'G' : {
        'A' : {
            'UC' : [ 'D', 'Asp', 'Aspartic acid' ],
            'AG' : [ 'E', 'Glu', 'Glutamic acid' ],
        },
        'C' : {
            'UCAG' : [ 'A', 'Ala', 'Alanine' ],
        },
        'G' : {
            'UCAG' : [ 'G', 'Gly', 'Glycine' ],
        },
        'U' : {
            'UCAG' : [ 'V', 'Val', 'Valine' ],
        },
    },
}

