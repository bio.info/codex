#!/usr/bin/env python2.7

"""
Copyright (c) 2018, Giulio Piemontese, <gpiemont@github.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by Giulio Piemontese.
4. Neither the name of Giulio Piemontese nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import os, sys, logging, operator

# Translation table

import genetic_code

logging.basicConfig(
    format='%(levelname)s: %(asctime)s : %(message)s',
    level=logging.INFO,
    datefmt='%d/%m/%Y %I:%M:%S %p'
)

#
#   Genetic code's State Machine
#       internal states
#

ring0 = None
ring1 = None
ring2 = None

#
#   Helper function
#

def code(aa):
    return aa[0]

def short(aa):
    return aa[1]

def fullname(aa):
    return aa[2]

def isstop(aa):
    return aa[0] == '*' 

#
#   State advance function
#

def nextbase(rnaseq, pos, aa=[], name="code"): 
     
    base = rnaseq[pos]

    # Global SM states
    global ring0, ring1, ring2

    try:
        if not ring0:
            ring0 = genetic_code.sm[base]
            logging.debug("[RING] Found ring0 : {}".format(base))
            return pos + 1

        if not ring1:
            ring1 = ring0[base]
            logging.debug("[RING0] Found ring1 : {}".format(base))
            return pos + 1

        if not ring2:
            size = 0

            for last in ring1.keys():
                logging.debug("[RING1] Trying.. '{}'".format(last)) 
                    
                try:
                    if base in last:
                        try:
                            ring2 = ring1[last]
                            logging.debug("[RING1] Found ring2 : {} (Fullname : {})".format(code(ring2), fullname(ring2)))
                        except:
                            return pos

                        if name == "code":
                            aa.append(code(ring2))
                        elif name == "short":
                            aa.append(short(ring2))
                        elif name == "full":
                            aa.append(fullname(ring2))
                      
                        if isstop(ring2):
                            # XXX
                            break
                 
                        # Reset SM states
                        ring0, ring1, ring2 = None, None, None
                        return pos + 1
                        
                    continue

                except IndexError as e:
                    logging.error("[RING1] No such '{}' in '{}' seq (pos=={}): {}".format(base, last, pos, str(e)))
                    return len(rnaseq)


    except KeyError:
        logging.error("[RING] RNA pos: {}".format(pos))
        ring0, ring1, ring2 = None, None, None
        return len(rnaseq)

    # Reset SM states
    ring0, ring1, ring2 = None, None, None

    return len(rnaseq)

def rna_to_codon(rnaseq="", type="code"):

    rna = list(rnaseq)

    code    = []
    short   = []
    full    = []

    response = {
        "code" : [],
        "short" : [],
        "full"  : []
    }

    ring0 = None
    ring1 = None
    ring2 = None

    pos = 0

    if type == "code" or type == "all":
        while pos != None and not pos == len(rnaseq):
            pos = nextbase(rnaseq, pos, aa=code, name="code")
            logging.debug("[RNA-TO-CODON] RNA  : {}".format(rnaseq[0:pos]))
            logging.debug("[RNA-TO-CODON] AA  : {}".format("-".join(code)))

        response["code"] = code
        pos = 0

    logging.info("[RNA-TO-CODON] pos : {}".format(pos))
    if type == "short" or type == "all":
        while pos != None and not pos == len(rnaseq):
            pos = nextbase(rnaseq, pos, aa=short, name="short")
            logging.debug("[RNA-TO-CODON] RNA  : {}".format(rnaseq[0:pos]))
            logging.debug("[RNA-TO-CODON] AA  : {}".format("-".join(short)))

        response["short"] = short
        pos = 0

    logging.info("[RNA-TO-CODON] pos : {}".format(pos))
    if type == "full" or type == "all":
        while pos != None and not pos == len(rnaseq):
            pos = nextbase(rnaseq, pos, aa=full, name="full")
            logging.debug("[RNA-TO-CODON] RNA  : {}".format(rnaseq[0:pos]))
            logging.debug("[RNA-TO-CODON] AA  : {}".format("-".join(full)))
        
        response["full"] = full
        pos = 0

    logging.info("[RNA-TO-CODON] pos : {}".format(pos))
    return response

if __name__ == "__main__":
 
    rna = str()

    if len(sys.argv) > 1:
        logging.info("Reading from command line arguments..")
        rna = sys.argv[1]
    elif os.path.exists('Input.rna'):
        logging.info("Reading from 'Input.rna' in current working directory")
        with open('Input.rna', 'r') as data:
            rna = data.read()
    else:
        logging.warning("No input file 'Input.rna' in current working directory, reading from stdin")
        rna = raw_input()

    logging.info("[MAIN] RNA Sequence = {}".format(list(rna)))

    aa = rna_to_codon(rnaseq=rna, type="all")

    print "AA Sequence (code)  : " + str(aa["code"])
    print "AA Sequence (short) : " + "-".join(aa["short"])
    print "AA Sequence (full)  : " + "-".join(aa["full"])
